#define KOIOS_CONFNAME ".koios.cfg"

typedef struct {
	char *name;
	int tag_flags, directory_flags;
} Mode;

typedef struct {
	char *change_dir, *set_config, *include_path;
	int follow_links, show_tags, recurse_depth, strict;
	int add_tagnames, del_tagnames, verbose;
	char **taglist;
} Settings;

typedef struct {
	koios_state kstate;
	koios_mask include, exclude;
	int set_include, set_exclude;
	magic_t magic_cookie;
	char *magic_match;
	char **taglist, **filelist;
} State;
