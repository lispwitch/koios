CC=gcc
CFLAGS= -D_XOPEN_SOURCE=700 -D_FORTIFY_SOURCE=2 -std=c99 -Wall -Wextra -pedantic -O0 -fstack-protector-strong -fno-omit-frame-pointer
CPPCHKFLAGS= --std=posix --std=c99 --platform=unix64 --enable=all --suppress=missingIncludeSystem
SPARSEFLAGS= -Wsparse-all -gcc-base-dir `gcc --print-file-name=` 
SCANBUILDFLAGS= -enable-checker core -enable-checker security -enable-checker unix
SCANBUILDCMD= `{ (command -v scan-build-7) || (command -v scan-build) || echo "" } 
SCANBUILD= `{ [ -z "$SCANBUILDCMD" ] && echo "" || echo "$SCANBUILDCMD $SCANBUILDFLAGS" }

FILES=  `{ls *.c}
OBJS=   ${FILES:%.c=%.o}
MAN=    docs/koios.1
LIBRARY=./libkoios/libkoios.a
LIBS=   -lkoios -lmagic -lrt -lm 
INCS=   -I./libkoios/ -L./libkoios/

DESTDIR=
PREFIX=  /usr/local
BINDIR=  $DESTDIR$PREFIX/bin/
MANDIR=  $DESTDIR$PREFIX/man/man1/

TESTFILE=tmp-fs.ext4
TESTMOUNT=tmpmnt
# Changing this value to a lower value causes an insufficient space error
# in some of the tests, unfortunately.
TESTSIZE= `{ echo $(( 1024*256 )) }

build:Q: koios
  :

koios: $LIBRARY $OBJS
  $SCANBUILD $CC $CFLAGS $INCS $OBJS $LIBS -o $target

%.o:
  $SCANBUILD $CC -c $CFLAGS $INCS $stem.c $LIBS 

./libkoios/libkoios.a:
  cd libkoios && exec mk

lint:V:
  sparse $SPARSEFLAGS $INCS $FILES
  cppcheck $CPPCHKFLAGS --force $FILES

test:V: setup_test do_test nuke_test

setup_test:V:
  [ -e $TESTMOUNT ] && sudo umount $TESTMOUNT
  [ -e $TESTMOUNT ] && rm -rf $TESTMOUNT
  [ -e $TESTFILE ] && rm $TESTFILE
  mkdir $TESTMOUNT
  : # Create 64MiB file
  dd if=/dev/zero of=$TESTFILE bs=$TESTSIZE count=1
  : # Make the filesystem (with 1024 bytes of extended attributes)
  mkfs.ext3 -b 1024 $TESTFILE
  : # Mount the filesystem
  sudo mount $TESTFILE $TESTMOUNT -o user_xattr
  : # Ensure that we can read/write to the tmpfs
  sudo chown $USER:$USER $TESTMOUNT

do_test:V:
  cram tests

nuke_test:V:
  [ -e $TESTMOUNT ] && sudo umount $TESTMOUNT
  [ -e $TESTFILE ] && rm $TESTFILE
  [ -e $TESTMOUNT ] && rm -rf $TESTMOUNT

clean:V:
  rm -f *.o koios

nuke:V: clean
  cd libkoios && exec mk clean

install: koios
  # Move the binaries
  install -d $BINDIR
  install -m 755 koios $BINDIR
  # Move the manual
  install -d $MANDIR
  install -m 644 $MAN $MANDIR

uninstall: 
  rm -rf $BINDIR/koios
  rm -rf $MANDIR/$MAN
  :
