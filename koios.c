#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdarg.h>
#include <ftw.h>
#include <magic.h>

#include <libkoios.h>
#include "koios.h"

#include <assert.h>

#define NOPENFD 32

enum {
	MODE_DO,
	MODE_INIT,
	MODE_SHOW,
	MODE_TAGS,
	MODE_AUTO,
	MODE_PURGE,
	MODE_RENAME,
	NUMMODES,

	SOFTTAGS   = 2,  /* Don't fail if no tags were given */
	TAGS       = 1,  /* Fail if no tags were given */
	NOTAGS     = 0,  /* No tags are provided */
	FILELIST   = 1,  /* Command accepts a file list */
	NOFILELIST = 0,  /* Command doesn't accept a file list */

	SUCCESS    =  1, /* These two macros are basically only used in */
	FAILURE    = -1, /* find_config */
};

Mode modes[NUMMODES] = {
	{"do",       TAGS,   FILELIST},
	{"init",   NOTAGS, NOFILELIST},
	{"show", SOFTTAGS,   FILELIST},
	{"tags",     TAGS, NOFILELIST},
	{"auto",     TAGS,   FILELIST},
	{"purge",  NOTAGS,   FILELIST},
	{"rename", NOTAGS, NOFILELIST},
};

Settings setting = {
	.change_dir    = NULL,  /* Handled in main */
	.set_config    = NULL,  /* Handled in load_config and save_config */
	.follow_links  = 0,     /* Handled in traverse */
	.include_path  = NULL,  /* Handled in do_init */
	.show_tags     = 0,     /* Handled in do_show */
	.recurse_depth = 0,     /* Handled in do_show | do_purge | do_tag */
	.strict        = 0,     /* Handled in parse_options */
	.add_tagnames  = 0,     /* Handled in modify_tagnames */
	.del_tagnames  = 0,     /* Handled in modify_tagnames */
	.verbose       = 0,     /* Handled in wprint */
};


/* Using the ftw library means that we can't pass values to the file tree
   handlers. This is a Not Great workaround */
State state = {
	.kstate = {0},
	.include = {NULL},
	.exclude = {NULL},
	.set_include = 0,
	.set_exclude = 0,
	.magic_cookie = NULL,
	.magic_match = NULL,
	.taglist = NULL,
	.filelist = NULL
};

const char *usage = "koios [do] [OPTIONS] [TAGS] <file/directory list>\n"
                    "koios init [OPTIONS] <path>\n"
		    "koios tags [TAGS]\n"
	            "koios show [OPTIONS] [TAGS] <file/directory list>\n"
		    "koios auto [OPTIONS] <mime string> [TAGS] <file/directory list>\n"
	            "koios purge [OPTIONS] <file/directory list>\n"
		    "koios rename [OPTIONS] <tagname> <tagname>\n";


#define OPTION(s, short, long) \
	((strcmp(s, short)) == 0 || (strcmp(s, long)) == 0)

#define eprint(...) \
	fprintf(stderr, __VA_ARGS__)

#define wprint(...) \
	if (setting.verbose) { fprintf(stderr, __VA_ARGS__); }

#define THROW(n) \
	{ status = n; goto _throw; }


static int
parse_num(char *s) /* atoi but with clear semantics */
{
	size_t i;
	int n = 0;

	if (!s) { return -1; }
	for (i = 0; s[i] && isdigit(s[i]); i++) {
		n *= 10;
		n += (s[i] - '0');
	}
	if (s[i] && !isdigit(s[i])) { return -1; }
	return n;
}


static int
parse_options(char **argv, int mode, int i)
{
	for (; argv[i]; i++) {
		if ((strcmp(argv[i], "--")) == 0) {
			i++;
			break;
		}
		else if (OPTION(argv[i], "-C", "--change")) {
			if (!argv[i+1]) {
				eprint("Missing argument to %s\n", argv[i]);
				return -1;
			}
			setting.change_dir = argv[++i];
		}
		else if (OPTION(argv[i], "-c", "--config")) {
			if (!argv[i+1]) {
				eprint("Missing argument to %s\n", argv[i]);
				return -1;
			}
			setting.set_config = argv[++i];
		}
		else if (OPTION(argv[i], "-l", "--links")) {
			setting.follow_links = 1;
		}
		else if (OPTION(argv[i], "-i", "--include")) {
			if (mode != MODE_INIT) {
				printf("%s argument only valid in mode init\n",
				       argv[i]);
				return -1;
			}
			if (!argv[i+1]) {
				eprint("Missing argument to %s\n", argv[i]);
				return -1;
			}
			setting.include_path = argv[++i];
		}
		else if (OPTION(argv[i], "-t", "--tags")) {
			if (mode != MODE_SHOW) {
				printf("%s argument only valid in mode show\n",
				       argv[i]);
				return -1;
			}
			setting.show_tags = 1;
		}
		else if (OPTION(argv[i], "-d", "--depth")) {
			if (!argv[i+1]) {
				eprint("Missing argument to %s\n", argv[i]);
				return -1;
			}
			setting.recurse_depth = parse_num(argv[++i]);
			if (setting.recurse_depth < 0) {
				eprint("depth argument is not a valid whole number\n");
			}
		}
		else if (OPTION(argv[i], "-s", "--strict")) {
			setting.strict = 1;
		}
		else if (OPTION(argv[i], "-v", "--verbose")) {
			setting.verbose = 1;
		}
		else if (OPTION(argv[i], "-h", "--help")) {
			printf("%s", usage);
			return -1;
		}
		else {
			if (argv[i][0] == '-' && setting.strict) {
				eprint("Malformed argument: %s\n", argv[i]);
				return -1;
			}
			else {
				break;
			}
		}
	}
	return i;
}

/* result  > 0 if we parse tags
 * result == 0 if we don't find any tags
 * result  < 0 on error */
static int
parse_tags(char **argv, int i, int strict)
{
	int j;
	if (!argv || i < 0) {
		eprint("parse_tags given null argument!\n");
		return -1;
	}

	for (j = 0; argv[i+j]; j++) {
		if (argv[i+j][0] == '+' || argv[i+j][0] == '-') {
			if (!koios_name_validate(argv[i]+1)) {
				eprint("Malformed tag name: %s\n", argv[i]);
				return -1;
			}
		}
		else {
			if (j == 0) {
				if (strict) {
					eprint("No tags were given\n");
					return -1;
				}
				else {
					wprint("No tags were given\n");
					return 0;
				}
			}
			break;
		}
		/* Else we assume it's a file/folder */
	}
	return i+j;
}


static int
set_mode(char *arg, int *i)
{
	int j;
	for (j = 0; j < NUMMODES; j++) {
		if ((strcmp(arg, modes[j].name)) == 0) {
			if (i) { *i += 1; }
			return j;
		}
	}
	return MODE_DO;
}


/* TODO: Replace this chunk of code with something clearer

   Unfortunately because you can't resolve file descriptors to
   pathnames this is the only _portable_ way of doing this :(
   Actually, even _this_ isn't strictly portable because PATH_MAX can
   in some cases be bogus. This is basically the least-crap way of
   doing this, but even _then_ this is still a heap of crap. SIGH. */
static int
find_config(char **result)
{
	char origpath[PATH_MAX] = {0};
	char path[PATH_MAX+1+(sizeof KOIOS_CONFNAME)] = {0};
	char *s = NULL;
	int fd = -1;
	int status = 0;

	if (!getcwd(origpath, PATH_MAX-1)) {
		perror("Unable to read path to current directory");
		return -1;
	}
	memcpy(path, origpath, PATH_MAX);

	while (path[1] != '\0') {
		/* /x/y/z -> /x/y/z/.koioscfg */
		s = path+strlen(path);
		s[0] = '/';
		memcpy(s+1, KOIOS_CONFNAME, strlen(KOIOS_CONFNAME)+1);


		/* Try open("/x/y/z/.koioscfg", ...) */
		wprint("Testing that config is at %s\n", path);
		if ((fd = open(path, O_RDONLY)) > -1) {
			*result = strdup(path);
			wprint("Config is at %s\n", path);
			THROW(SUCCESS);
		}

		/* /x/y/z -> /x/y */
		if ((chdir("..")) < 0) {
			eprint("Error recursing upwards\n");
		}
		if (!getcwd(path, PATH_MAX)) {
			perror("Unable to get current path");
			THROW(FAILURE);
		}
	}

	/* This code duplication is icky, it feels like there is a better way
	   to do this on the whole */
	wprint("Testing that config is at %s\n", "/"KOIOS_CONFNAME);
	if ((fd = open("/"KOIOS_CONFNAME, O_RDONLY)) > -1) {
		*result = strdup("/"KOIOS_CONFNAME);
		wprint("Config is at %s\n", "/"KOIOS_CONFNAME);
		THROW(SUCCESS);
	}

_throw:
	if ((chdir(origpath)) < 0) {
		eprint("Error: Unable to move back into '%s'\n", origpath);
		status = -1;
	}
	return status;
}


#define mkpath(...) mkpath_(__VA_ARGS__, (char*)NULL)
static char *mkpath_(char *s, ...)
{
	char *s_orig = s;
	char *s_next = NULL;
	char *d = NULL;
	size_t length = 0;
	size_t offset = 0;
	va_list ap;

	if (!s) {
		eprint("Error creating path.\n");
		return NULL;
	}

	/* find length of path plus how many slashes to add */
	va_start(ap, s);
	while (s) {
		size_t size, slash;
		s_next = va_arg(ap, char *);
		size = strlen(s);
		slash = (s_next && s[size] != '/' && s_next[0] != '/');

		length += size+slash;
		s = s_next;
	}
	va_end(ap);

	s = s_orig;
	if (!(d = calloc(length+1, 1))) {
		perror("calloc");
		eprint("Error creating path\n");
		return NULL;
	}

	/* Join the paths together */
	va_start(ap, s);
	do {
		size_t size, slash;

		s_next = va_arg(ap, char *);
		size = strlen(s);
		/* NB: Possible bug? we do (size-1) not size here */
		slash = (s_next && s[size-1] != '/' && s_next[0] != '/');

		memcpy(d+offset, s, size);
		if (slash) { d[offset+size] = '/'; }
		offset += size+slash;
		s = s_next;
	} while (s);
	va_end(ap);
	d[offset] = '\0';

	return d;
}


/* stdarg.h abuse, I guess. But this seems like the cleanest way to do this */
#define find_user_config(...) find_user_config_(__VA_ARGS__, (char*)NULL)
char *find_user_config_(char *path, ...)
{
	char *result = NULL;
	va_list ap;

	va_start(ap, path);
	while (path) {
		if (strlen(path) == 0) {
			path = va_arg(ap, char *);
			continue;
		}
		if ((access(path, F_OK)) == 0 && !result) {
			result = path;
		}
		else {
			free(path);
		}
		path = va_arg(ap, char *);
	}
	va_end(ap);

	return result;
}


/* Returns false if the path doesn't exist, or is a file */
int init_isdir(char *path)
{
	struct stat st = {0};
	if (!path) {
		wprint("Expected a value to be passed and one wasn't?\n");
		return 0;
	}
	if ((stat(path, &st)) >= 0) {
		return S_ISDIR(st.st_mode);
	}
	else if (errno != ENOENT) {
		/* If it doesn't exist, we're likely expected to create it */
		perror(path);
	}
	return 0;
}


char *select_user_config(char *path)
{
	int status = 0;
	char *retpath = NULL;

	if (setting.set_config) {
		return strdup(setting.set_config);
	}

	/* Try and find a config file in the current tree */
	if ((status = find_config(&retpath)) < 0) {
		if (retpath) { free(retpath); }
		return NULL;
	}
	else if (status > 0) {
		return retpath;
	}

	/* Otherwise if we weren't given a path, search for a config file in
	   the user's config directories */
	if (!path) {
		char *xdg_confdir = getenv("XDG_CONFIG_HOME");
		char *home = getenv("HOME");
		path = find_user_config(xdg_confdir ? mkpath(xdg_confdir) : "",
		                        home ? mkpath(home, ".config") : "",
	                                home ? mkpath(home) : "");
		if (!path) {
			perror("Unable to construct a valid path");
			if (errno) { perror("mkpath"); }
			return NULL;
		}
		retpath = mkpath(path, KOIOS_CONFNAME);
		free(path);
		return retpath;
	}

	/* If we're given a path and it's NOT a dir and doesn't exist, we
	   should make it from scratch! */
	if (init_isdir(path)) {
		if (!(retpath = mkpath(path, KOIOS_CONFNAME))) {
			perror("Unable to construct a valid path!");
			return NULL;
		}
		return retpath;
	}

	/* The alternative is trying to figure out if the input is freeable,
	   or adding a flag or something. This is less work */
	return strdup(path);
}


int save_config(koios_state *state, char *path)
{
	int status;
	char *newpath = select_user_config(path);

	wprint("Writing config file to %s\n", newpath);

	if ((status = koios_cfg_store(state, newpath)) < 0) {
		eprint("Unable to save config file: %s\n",
		       koios_errstr(status));
		free(newpath);
		return -1;
	}

	free(newpath);
	return 0;
}


int load_config(koios_state *state)
{
	int status = 0;
	char *path = select_user_config(NULL);

	wprint("Loading config file from %s\n", path);

	if ((status = koios_cfg_open(state, path)) < 0) {
		eprint("Unable to open config file %s: %s\n",
		       path, koios_errstr(status));
		free(path);
		return -1;
	}

	free(path);
	return 0;
}


/* Generate config file from the current path, copy over the tags to include,
 * then figure out where to write the config file */
int do_init(koios_state *state, char *path)
{
	int status;

	errno = 0;
	fflush(stdout);
	if ((status = koios_cfg_from_native(state)) < 0) {
		eprint("Unable to generate config file: %s\n",
		       koios_errstr(status));
		if (errno > 0) { perror(""); }
		return -1;
	}

	/* Copy over tags from the included file */
	if (setting.include_path) {
		koios_state srctags = {0};
		if ((status = koios_cfg_open(&srctags, setting.include_path)) < 0) {
			eprint("Could not include %s: %s%s",
			       setting.include_path,
			       koios_errstr(status),
			       errno ? ": " : "");
			if (errno) { perror(""); }
			return -1;
		}

		/* We can abstract this out better in the koios interface later (TODO) */
		memcpy(state->tag_names, srctags.tag_names, state->bits_per_blocksize);

		koios_cfg_close(&srctags);
	}

	if ((status = save_config(state, path)) < 0) {
		return status;
	}

	return 1;
}


int display_tags(const char *path)
{
	int status = 0;

	koios_mask mask = {NULL};
	koios_tag tag = 0;

	if ((status = koios_mask_new(&state.kstate, &mask)) < 0) {
		eprint("Unable to open tag mask: %s\n", koios_errstr(status));
		goto _throw;
	}

	if ((status = koios_mask_load(&state.kstate, &mask, path)) < 0) {
		eprint("Unable to load tag mask from %s: %s\n",
		       path, koios_errstr(status));
		goto _throw;
	}

	while ((status = koios_mask_poptag(&state.kstate, &mask, &tag)) > 0) {
		char *tagname = NULL;
		if ((status = koios_tag_fetchname(&state.kstate, tag, &tagname)) > 0) {
			printf("%s ", tagname);
		}
		else if (status < 0) {
			eprint("Failure while retrieving tagname: %s\n",
			       koios_errstr(status));
		}
	}
	if (status < 0) {
		eprint("Failure while attempting to print tags: %s\n",
		       koios_errstr(status));
		goto _throw;
	}

	koios_mask_del(&mask);

	return 0;
_throw:
	koios_mask_del(&mask);
	return status;
}


int build_mask(koios_state *state, koios_mask *mask, char **taglist, char c, int *did_tag)
{
	int status = 0;
	size_t i;
	if (!state || !mask || !c) {
		eprint("Error: build_mask was given a NULL argument\n");
		return -1;
	}
	
	if (!taglist) {
		return 0;
	}

	for (i = 0; taglist[i] && (taglist[i][0] == '-' || taglist[i][0] == '+'); i++) {
		if (taglist[i][0] == c) {
			koios_tag tag = 0;
			if ((status = koios_name_find(state, taglist[i]+1, &tag)) < 0) {
				eprint("Error while looking up tag name: %s\n",
				       koios_errstr(status));
				break;
			}
			else if (status == 0) {
				eprint("Unable to find tag name: %s\n", taglist[i]+1);
				break;
			}

			if ((status = koios_tag_addtomask(state, mask, tag)) < 0) {
				eprint("Error while building mask: %s\n",
				       koios_errstr(status));
				break;
			}
			*did_tag = 1;
		}
	}

	return status;
}


int ftw_whine(const char *path, int typeflag, const struct stat *st)
{
	if (typeflag == FTW_DNR) {
		eprint("%s: Unable to read directory\n", path);
		return 1;
	}
	else if (typeflag == FTW_NS) {
		eprint("%s: Failed to stat\n", path);
		perror("stat");
		return 1;
	}
	else if (typeflag == FTW_SLN) {
		eprint("%s: Broken symbolic link\n", path);
		return 1;
	}
	else if (!st) {
		eprint("%s: Failed to stat\n", path);
		perror("stat");
		return 1;
	}
	return 0;
}


int do_purge(const char *path, const struct stat *st, int typeflag, struct FTW *ftwbuf)
{
	if (setting.recurse_depth && ftwbuf && ftwbuf->level > setting.recurse_depth) {
		return 0;
	}

	if (!ftw_whine(path, typeflag, st)) {
		int status;
		wprint("Purging %s\n", path);
		if ((status = koios_mask_strip(&state.kstate, path)) < 0) {
			eprint("Error purging %s: %s\n",
			       path,
			       koios_errstr(status));
		}
	}
	return 0;
}


int filter_out_file(const char *path) {
	int status = 0;
	int include = 0;
	int exclude = 0;
	koios_mask mask = {NULL};

	if ((status = koios_mask_new(&state.kstate, &mask)) < 0) {
		eprint("Unable to generate new mask: %s\n",
		       koios_errstr(status));
		return 1;
	}

	if ((status = koios_mask_load(&state.kstate, &mask, path)) < 0) {
		wprint("Unable to load mask from file %s: %s\n",
			path, koios_errstr(status));
		goto _throw;
	}

	/* if we weren't given any tags, and koios_mask_load has a tag mask,
	   don't filter out the file */
	if (!state.taglist[0] && status >= 0) {
		return 0;
	}

	if (state.set_include &&
	    (include = koios_mask_issubset(&state.kstate, &mask, &state.include)) < 0) {
		eprint("Error checking mask: %s\n", koios_errstr(include));
		goto _throw;
	}

	if (state.set_exclude &&
	    (exclude = koios_mask_issubset(&state.kstate, &mask, &state.exclude)) < 0) {
		eprint("Error checking mask: %s\n", koios_errstr(exclude));
		goto _throw;
	}

	if ((status = koios_mask_del(&mask)) < 0) {
		eprint("Error while freeing mask: %s\n", koios_errstr(status));
		goto _throw;
	}

	return exclude || !include;

_throw:
	if ((status = koios_mask_del(&mask)) < 0) {
		eprint("Error while freeing mask: %s\n", koios_errstr(status));
	}
	return 1;
}


int apply_changes(const char *path)
{
	int status = 0;
	koios_mask mask = {NULL};

	if ((status = koios_mask_new(&state.kstate, &mask)) < 0) {
		eprint("Unable to generate new mask: %s\n",
		       koios_errstr(status));
		return 1;
	}

	if ((status = koios_mask_load(&state.kstate, &mask, path)) < 0) {
		wprint("Unable to load mask from file %s: %s\n",
			path, koios_errstr(status));
	}

	if ((status = koios_mask_merge(&state.kstate, &mask, &state.include)) < 0) {
		eprint("Unable to merge changes into mask: %s\n",
			koios_errstr(status));
		goto _throw;
	}

	if ((status = koios_mask_exclude(&state.kstate, &mask, &state.exclude)) < 0) {
		eprint("Unable to merge changes into mask: %s\n",
			koios_errstr(status));
		goto _throw;
	}

	if ((status = koios_mask_save(&state.kstate, &mask, path)) < 0) {
		eprint("Unable to save mask to file %s: %s\n",
		       path, koios_errstr(status));
		goto _throw;
	}

	if ((status = koios_mask_del(&mask)) < 0) {
		eprint("Error while freeing mask: %s\n", koios_errstr(status));
		goto _throw;
	}

	return 0;
_throw:
	if ((status = koios_mask_del(&mask)) < 0) {
		eprint("Error while freeing mask: %s\n", koios_errstr(status));
	}
	return -1;
}


int do_show(const char *path, const struct stat *st, int typeflag, struct FTW *ftwbuf)
{
	if (setting.recurse_depth && ftwbuf && ftwbuf->level > setting.recurse_depth) {
		return 0;
	}

	if (!ftw_whine(path, typeflag, st) && (!state.taglist || !filter_out_file(path))) {
		printf("%s", path);
		if (setting.show_tags) {
			printf(": ");
			display_tags(path);
		}
		fputc('\n', stdout);
	}

	return 0;
}


int do_tag(const char *path, const struct stat *st, int typeflag, struct FTW *ftwbuf)
{
	if (setting.recurse_depth && ftwbuf && ftwbuf->level > setting.recurse_depth) {
		return 0;
	}

	if (!ftw_whine(path, typeflag, st)) {
		if ((apply_changes(path)) < 0) {
			return 0;
		}
		wprint("Applied tags to %s\n", path);
	}

	return 0;
}


int init_libmagic(void)
{
	wprint("Initializing libmagic\n");
	if (!(state.magic_cookie = magic_open(MAGIC_MIME))) {
		eprint("Unable to init libmagic\n");
		return -1;
	}
	wprint("Loading libmagic database\n");
	if ((magic_load(state.magic_cookie, NULL)) < 0) {
		 eprint("Unable to load libmagic database: %s\n",
		        magic_error(state.magic_cookie));
		magic_close(state.magic_cookie);
		return -1;
	}

	return 0;
}


void deinit_libmagic(void)
{
	magic_close(state.magic_cookie);
}


int do_autotag(const char *path, const struct stat *st, int typeflag, struct FTW *ftwbuf)
{
	if (setting.recurse_depth && ftwbuf && ftwbuf->level > setting.recurse_depth) {
		return 0;
	}

	if (!ftw_whine(path, typeflag, st)) {
		const char *magic_full = magic_file(state.magic_cookie, path);
		if (!magic_full) { return 0; }
		wprint("%s: libmagic returned MIME string '%s'\n", path, magic_full);
		if (!strstr(magic_full, state.magic_match) || (apply_changes(path)) < 0) {
			return 0;
		}
		wprint("Applied tags to %s\n", path);
	}

	return 0;
}


int traverse(char **filelist, int (*action)(const char *, const struct stat *,
                                            int, struct FTW *))
{
	int flags = FTW_MOUNT | (setting.follow_links ? FTW_PHYS : 0);
	struct stat st = {0};
	struct FTW ftwbuf = {
		.base = 0,
		.level = 0
	};

	if ((build_mask(&state.kstate, &state.include, state.taglist, '+', &state.set_include)) < 0) {
		return 0;
	}
	if ((build_mask(&state.kstate, &state.exclude, state.taglist, '-', &state.set_exclude)) < 0) {
		return 0;
	}

	for (; *filelist; filelist++) {
		if ((stat(*filelist, &st)) < 0) {
			perror(*filelist);
			continue;
		}

		if (S_ISREG(st.st_mode)) {
			action(*filelist, &st, FTW_F, &ftwbuf);
			continue;
		}

		if ((nftw(*filelist, action, NOPENFD, flags)) < 0) {
			perror("nftw");
		}
	}

	return 0;
}


int show_tag(const char *string, void *data)
{
	if (string) {
		printf("%s\n", string);
	}
	return 0;
}


void show_tagnames(void)
{
	int status = 0;
	koios_tag dummy = 0;
	/* Some minor abuse of the search function :) */
	if ((status = koios_name_search(&state.kstate, &dummy, show_tag, NULL)) < 0) {
		eprint("Listing tags failed: %s\n", koios_errstr(status));
	}
}


void modify_tagnames(char **taglist)
{
	int status;
	size_t i = 0;
	koios_tag dummy;

	for (i = 0; taglist[i] && (taglist[i][0] == '-' || taglist[i][0] == '+'); i++) {
		if (taglist[i][0] == '+') {
			wprint("adding tag: %s\n", taglist[i]);
			if ((status = koios_name_find(&state.kstate, taglist[i]+1, &dummy)) < 0) {
				eprint("Error while searching for name %s: %s\n",
				        taglist[i]+1,
					koios_errstr(status));
			}
			else if (status == 0) {
				if ((koios_name_set(&state.kstate, NULL, taglist[i]+1)) < 0) {
					eprint("Error while adding name to tag list: %s\n",
					        koios_errstr(status));
				}
			}
		}
		else if (taglist[i][0] == '-') {
			wprint("removing tag: %s\n", taglist[i]);
			if ((status = koios_name_set(&state.kstate, taglist[i]+1, "")) < 0) {
				eprint("Error while removing tag %s: %s\n",
				       taglist[i]+1,
				       koios_errstr(status));
			}
		}
	}
}


int main(int argc, char **argv)
{
	int status = 0;
	int strict = 0;
	int mode, j = 0, i = 1;

	if (!argv[1]) { printf("%s", usage); exit(1); } 

	mode = set_mode(argv[1], &i);

	if ((i = parse_options(argv, mode, i)) < 0) { exit(1); }

	/* If the mode is auto, skip the first entry before the tags are given */
	if (mode == MODE_AUTO) {
		/* Assignment here is intended, don't worry */
		if (!(state.magic_match = argv[i])) {
			eprint("Missing MIME match string\n");
			exit(1);
		}
		i++;
	}

	/* If we expect the args to contain tag flags, then validate them and
	 * determine the end of them.
	 * Otherwise if the mode is rename, validate the arguments to ensure
	 * they are proper tag names */

	strict = modes[mode].tag_flags != SOFTTAGS;
	if (modes[mode].tag_flags && (j = parse_tags(argv, i, strict)) < 0) {
		exit(1);
	}
	else if (mode == MODE_RENAME) {
		if (!(argv[i] && argv[i+1])) {
			eprint("Missing argument\n");
			exit(1);
		}
		if (((koios_name_validate(argv[i])) < 1)
		 || ((koios_name_validate(argv[i+1])) < 1)) {
		 	eprint("Malformed tag name\n");
			exit(1);
		}
	}

	state.filelist = (modes[mode].tag_flags && j != 0) ? argv+j : argv+i;
	state.taglist  = (modes[mode].tag_flags && j != 0) ? argv+i : argv+argc;

	if (setting.change_dir) {
		if ((chdir(setting.change_dir)) < 0) {
			perror(setting.change_dir);
			exit(1);
		}
	}

	if (mode != MODE_INIT && (load_config(&state.kstate)) < 0) {
		eprint("Unable to load a valid config file\n");
		exit(1);
	}

	if (modes[mode].tag_flags) {
		state.set_include = 0;
		state.set_exclude = 0;
		if ((status = koios_mask_new(&state.kstate, &state.include)) < 0) {
			eprint("Unable to generate new include mask: %s\n",
			       koios_errstr(status));
			return 1;
		}
		if ((status = koios_mask_new(&state.kstate, &state.exclude)) < 0) {
			eprint("Unable to generate new exclude mask: %s\n",
			       koios_errstr(status));
			return 1;
		}
	}

	switch (mode) {
	case MODE_DO:
		traverse(state.filelist, do_tag);
		save_config(&state.kstate, NULL);
		break;
	case MODE_INIT:
		if ((do_init(&state.kstate, *(state.filelist))) < 0) {
			goto _throw;
		}
		break;
	case MODE_SHOW:
		traverse(state.filelist, do_show);
		break;
	case MODE_AUTO:
		if ((init_libmagic()) < 0) {
			goto _throw;
		}
		traverse(state.filelist, do_autotag);
		deinit_libmagic();
		break;
	case MODE_TAGS:
		if (!state.taglist[0]) {
			show_tagnames();
		}
		else {
			modify_tagnames(state.taglist);
			save_config(&state.kstate, NULL);
		}
		break;
	case MODE_PURGE:
		traverse(state.filelist, do_purge);
		break;
	case MODE_RENAME:
		koios_name_set(&state.kstate,
		               state.filelist[0], state.filelist[1]);
		save_config(&state.kstate, NULL);
		break;
	}

	if (modes[mode].tag_flags) {
		koios_mask_del(&state.include);
		koios_mask_del(&state.exclude);
	}
	koios_cfg_close(&state.kstate);
	return 0;

_throw:
	if (modes[mode].tag_flags) {
		koios_mask_del(&state.include);
		koios_mask_del(&state.exclude);
	}
	koios_cfg_close(&state.kstate);
	return 1;
}
