#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "extattr.h"
#include "libkoios.h"

static char *koios_errorstr[] = {
	"No error",
	"Unexpected NULL",
	"Error buffering file",
	"Bad config header",
	"Error while deriving options",
	"Problem during division",
	"Error while requesting more memory",
	"Error loading config",
	"Error while opening file",
	"Error while reading from file",
	"Not enough data read (from file)",
	"Invalid file descriptor given",
	"Filesystem unable to store attributes",
	"Error while deriving native options",
	"options are uninitialized",
	"Error while writing to file",
	"Unable to find requested tag",
	"Invalid name",
	"Maximum number of tags would be exceeded by this operation",
	"Size of proposed tag name exceeds builtin name size",
	"Tag out of bounds of mask",
	"Failed to strip tags from file",
	"Mask sizes are unequal, cannot compare",
	"Buffer is too small to hold number of set tags",
	"Setting extended attribute failed",
	"Getting extended attribute failed",
	"Removing extended attribute failed",
	"Stored tag is larger than default tag value",
	"No tag stored on file",
	"Unable to open RANDOMPATH",
	"Unable to read from RANDOMPATH",
	"Name does not exist",
	"Unable to create valid KOIOS_TESTFILE",
	"Tried to initialize an already initialized value",
	"Filesystem does not support extended attributes",
	"File does not exist or is not readable",
	"File does not exist or is not writable",
};

#define THROW(error) { status = (error); goto _throw; }

/*  Internal interface  */
static char           *loadfile        (const char *path);
static long            parse_field     (char *dp, long i, size_t *value, int endp);
static int             parse_head      (koios_state *state, char *data);
static int             derive_state    (koios_state *state);
static inline int      validnamec      (char c);
static inline int      validname       (const char *s);
static long            parse_name      (char *data, long i, koios_name *value);
static long            parse_names     (koios_state *state, long i, char *data);
static int             init_tagnames   (koios_state *state);
static int             test_xatsize    (char *s, size_t n);
static inline uint64_t load_int        (koios_mask *mask, size_t offset);
static inline void     save_int        (koios_mask *mask, size_t offset, uint64_t n);

/*  Internal definitions  */

static char *
loadfile(const char *path)
{
	int fd = 0;
	char *data = NULL;
	ssize_t len = 0;
	struct stat st = {0};

	if ((access(path, F_OK)) < 0) { return NULL; }

	if ((stat(path, &st)) < 0) { return NULL; }

	if ((fd = open(path, O_RDONLY)) < 0) { goto _throw; }

	if (!(data = calloc(st.st_size+1, 1))) { goto _throw; }

	if ((len = read(fd, data, st.st_size)) < 0) { goto _throw; }

	if (len < st.st_size) { goto _throw; }

	close(fd); /* there is nothing actionable we can do for failure, so don't care */

	return data;
_throw:
	if (fd < 0) { close(fd); }
	if (data) { free(data); }
	return NULL;
}


static long
parse_field(char *dp, long i, size_t *value, int endp)
{
	long j = 0;
	if (!dp || !value) { return -1; }

	for (; dp[i+j] && isdigit(dp[i+j]); j++) {
		*value *= 10;
		*value += (dp[i] - '0');
	}
	if (dp[i+j] && !(endp ? dp[i+j] == '!' : dp[i+j] == ':')) { return -1; }
	/* If we haven't progressed then fail */
	return !j ? -1 : i+j+1;
}


static int
parse_head(koios_state *state, char *data)
{
	ssize_t i = 0;

	if (data[i++] != '!') { return KERR_BADHEAD; }

	/* parse the header */
	if ((i = parse_field(data, i, &(state->version), 0))   < 0) { return KERR_BADHEAD; }
	if ((i = parse_field(data, i, &(state->blocksize), 1)) < 0) { return KERR_BADHEAD; }
	if (data[i++] != '\n') { return KERR_BADHEAD; }

	return i;
}


static int
derive_state(koios_state *state)
{
	int eflags = FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW | FE_UNDERFLOW;

	if (!state || state->bytes_per_int == 0 || state->blocksize == 0) { return KERR_NULL; }

	errno = 0;
	feclearexcept(FE_ALL_EXCEPT);

	state->ints_per_blocksize = llrint(ceil(state->blocksize / state->bytes_per_int));
	if (errno || fetestexcept(eflags)) { return KERR_BADOPTS; }

	state->bits_per_blocksize = state->blocksize * CHAR_BIT;

	return 0;
}


static inline int
validnamec(char c)
{
	return (c && (isalpha(c)
	          ||  isdigit(c)
	          ||  c == '_' || c == '#'
	          ||  c == ':' || c == '.'
	          ||  c == '@'
		  ));
}


static inline int
validname(const char *s)
{
	while (s && *s && validnamec(*s))
		s++;

	return (s && !*s);
}


static long
parse_name(char *data, long i, koios_name *value)
{
	ssize_t j = 0;
	char *name = NULL;

	if (!data || i < 0 || !value) { return -1; }

	name = value->name;
	for (; data[i] && validnamec(data[i]) && j < KOIOS_TAG_NAMSZ; i++, j++) {
		name[j] = data[i];
	}
	name[j] = '\0';
	if (j > 0) { value->used = 1; }
	if (!data[i] || (!validnamec(data[i]) && data[i] != '\n')) { return -1; }
	return ++i;
}


static long
parse_names(koios_state *state, long i, char *data)
{
	size_t j;
	if (!state || !state->tag_names|| !data || i < 0) { return KERR_NULL; }

	for (j = 0; j < state->bits_per_blocksize; j++) {
		if (data[i-1] == '\n' && !data[i]) { break; }
		if ((i = parse_name(data, i, &(state->tag_names[j]))) < 0) {
			return KERR_LOADERR;
		}
	}

	return j;
}


static int
init_tagnames(koios_state *state)
{
	if (!state || state->bits_per_blocksize == 0) { return KERR_NULL; }

	if (!(state->tag_names = calloc(state->bits_per_blocksize, sizeof(koios_name)))) {
		return KERR_BADALLOC;
	}
	return 0;
}


/*  External definitions  */


int
koios_cfg_open(koios_state *state, const char *path)
{
	int i = 0, status = 0;
	char *data = NULL;

	if (!state || !path) { return KERR_NULL; }
	if (!(data = loadfile(path))) { return KERR_FILEERR; }

	state->version = state->blocksize = 0;
	state->bytes_per_int = sizeof(uint64_t);
	state->ints_per_blocksize = state->bits_per_blocksize = 0;

	if ((i = parse_head(state, data)) < 0) { goto _throw; }

	if ((status = derive_state(state)) < 0) { goto _throw; }

	if ((status = init_tagnames(state)) < 0) { goto _throw; }

	while ((i = parse_names(state, i, data)) < 0) { goto _throw; }
	free(data);

	return 0;

_throw:
	if (data) { free(data); }
	koios_cfg_close(state);
	return i < 0 ? i : status;
}


int
koios_cfg_store(koios_state *state, const char *path)
{
	size_t i = 0;
	int fd = -1;	
	int status = 0;

	if (!state || !path || !state->tag_names) { return KERR_NULL; }
	
	if ((fd = open(path, O_CREAT|O_WRONLY, 0644)) < 0) { return KERR_BADOPEN; }

	if ((dprintf(fd, "!%lu:%lu!\n", state->version, state->blocksize)) < 0) {
		THROW(KERR_WRITESIZE);
	}

	for (; i < state->bits_per_blocksize; i++) {
		char *str = state->tag_names[i].used ? state->tag_names[i].name : "";
		if ((dprintf(fd, "%s\n", str)) < 0) { THROW(KERR_WRITESIZE); }
	}

_throw:
	if (fd > 0) { close(fd); }
	return status;
}


int
test_xatsize(char *bp, size_t n)
{
	if (!bp || !n) { return KERR_NULL; }

	if ((removexat(KOIOS_TESTFILE, KOIOS_TESTATTR)) < 0 && errno != ENOATTR) {
		return KERR_REMXAT;
	}

	if ((setxat(KOIOS_TESTFILE, KOIOS_TESTATTR, bp, n, 0)) < 0) {
		if (errno == ENOSPC || errno == ERANGE) {
			errno = 0;
			return 0;
		} else {
			return KERR_SETXAT;
		}
	}
	return 1;
}


int randfill(char *bp, size_t size)
{
	int status;
	int fd = -1;
	size_t i = 0;

	if ((fd = open(RANDOMPATH, O_RDONLY)) < 0) { THROW(KERR_RANDOPEN); }
	if ((read(fd, bp, size)) < 0) { THROW(KERR_RANDREAD); }
	/* ensure the buffer contains non-null data */
	for (i = 0; i < size; i++) {
		bp[i] = !bp[i] ? (bp[i]+1) : bp[i];
	}
	close(fd);
	return 0;
_throw:
	if (fd > 0) { close(fd); }
	return status;
}


long approx(char *bp, long max)
{
	long status;
	long min = 0;
	long x = max;
	long result = 0;

	if ((access(KOIOS_TESTFILE, F_OK)) < 0 || (access(KOIOS_TESTFILE, R_OK|W_OK)) < 0) {
		int fd = -1;
		if ((fd = open(KOIOS_TESTFILE, O_CREAT|O_RDWR, 0644)) < 0) {
			THROW(KERR_NOMKFILE);
		}
		close(fd);
	}

	while (!(result = test_xatsize(bp, max))) {
		max = x;
		x = (long)floor(x/2);
	}
	if (result < 0) { THROW(result); }

	/* TODO: This code is kind of convoluted and duplicated tbh. It could be DRYer */
	if (test_xatsize(bp, x) > 0 && !test_xatsize(bp, x+1)) {
		THROW(x);
	}

	while (labs(max - min) > 1) {
		x = (long)floorl(labs(max - min)/2) + min;
		if ((result = test_xatsize(bp, x)) < 0) {
			THROW(result);
		}
		!result ? (max = x) : (min = x);
	}
	if ((test_xatsize(bp, min+1))) { min++; }
	if ((unlink(KOIOS_TESTFILE)) < 0) { perror(KOIOS_TESTFILE); }
	return min;

_throw:
	return status;
}


int
koios_cfg_from_native(koios_state *state)
{
	int status = 0;
	char *bp = NULL;
	long result = 0;
	long max = maxxat();

	if (!state) { return KERR_NULL; }
	if (!(bp = calloc(max+1, 1))) { return KERR_BADALLOC; }
	if ((status = randfill(bp, max)) < 0) { THROW(status); }
	if ((result = approx(bp, max)) < 0) {
		THROW(result);
	} else {
		state->blocksize = result;
	}

	state->bytes_per_int = sizeof(uint64_t);
	state->version = KOIOS_FORMAT_VERSION;

	if ((status = derive_state(state)) < 0)  { goto _throw; }
	if ((status = init_tagnames(state)) < 0) { goto _throw; }

	return 0;

_throw:
	koios_cfg_close(state);
	if (bp) { free(bp); }
	return status;
}


int koios_cfg_from_given(koios_state *state, size_t blocksize, size_t version)
{
	int status = 0;

	if (!state || !blocksize) { return KERR_NULL; }

	state->blocksize = blocksize;
	state->bytes_per_int = sizeof(uint64_t);
	state->version = version;
	if ((status = derive_state(state)) < 0) { goto _throw; }
	if ((status = init_tagnames(state)) < 0) { goto _throw; }

	return 0;

_throw:
	koios_cfg_close(state);
	return status;
}


int
koios_cfg_validate(koios_state *state)
{
	int status = 0;
	koios_state native = {0};
	if ((status = koios_cfg_from_native(&native)) < 0) { return status; }

	status = state && state->version == native.version &&
	                  state->blocksize == native.blocksize;

	koios_cfg_close(&native);
	return status;
}


int koios_cfg_close(koios_state *state)
{
	if (!state) { return KERR_NULL; }
	if (state->tag_names) { free(state->tag_names); state->tag_names = NULL; }
	memset(state, (char)0, sizeof(koios_state));
	return 0;
}


int
koios_name_find(koios_state *state, const char *name, koios_tag *tag)
{
	size_t i = 0;
	koios_name *names = NULL;
	if (!state || !state->tag_names || !state->bits_per_blocksize || !tag || !name) {
		return KERR_NULL;
	}
	if (!validname(name)) { return KERR_BADNAME; }

	names = state->tag_names;

	for (; i < state->bits_per_blocksize; i++) {
		if (names[i].used && (strcmp(names[i].name, name)) == 0) {
			*tag = i;
			return 1;
		}
	}
	return 0;
}


int
koios_name_set(koios_state *state, const char *name, const char *newname)
{
	int status = 0;
	size_t i = 0;
	if (!state || !state->tag_names || !newname) { return KERR_NULL; }
	if ((name && !validname(name)) || !validname(newname)) { return KERR_BADNAME; }

	if (name) {
		if ((status = koios_name_find(state, name, &i)) < 0) {
			return status;
		}
		if (!status) { return KERR_NONAME; }
	}

	if (!name || !status) {
		while (i < state->bits_per_blocksize && state->tag_names[i].used) {
			i++;
		}
		if (i == state->bits_per_blocksize) { return KERR_MAXTAGS; }
	}

	memcpy(state->tag_names[i].name, newname, strlen(newname)+1);
	state->tag_names[i].used = 1;
	return 1;
}


int
koios_name_validate(const char *name)
{
	if (!name) { return KERR_NULL; }
	return validname(name);
}


int
koios_name_search(koios_state *state, koios_tag *tag, int (*cmp)(const char *str, void *data), void *data)
{
	size_t i = 0;
	koios_name *names = NULL;
	if (!state || !state->tag_names || !state->bits_per_blocksize || !tag || !cmp) {
		return KERR_NULL;
	}

	names = state->tag_names;

	for (; i < state->bits_per_blocksize; i++) {
		if (names[i].used && (cmp(names[i].name, data))) {
			*tag = i;
			return 1;
		}
	}
	return 0;
}


const char *
koios_errstr(int error)
{
	if (error > 0 || error < NUM_KERR) { return NULL; }
	return koios_errorstr[abs(error)];
}


static inline uint64_t load_int(koios_mask *mask, size_t offset)
{
	return ( (((uint64_t) mask->v[offset+0]) <<  0) | (((uint64_t)mask->v[offset+1]) <<  8) 
	       | (((uint64_t) mask->v[offset+2]) << 16) | (((uint64_t)mask->v[offset+3]) << 24)
	       | (((uint64_t) mask->v[offset+4]) << 32) | (((uint64_t)mask->v[offset+5]) << 40)
	       | (((uint64_t) mask->v[offset+6]) << 48) | (((uint64_t)mask->v[offset+7]) << 56) );
}

static inline void save_int(koios_mask *mask, size_t offset, uint64_t n)
{
	mask->v[offset+0] = (n >>  0);
	mask->v[offset+1] = (n >>  8);
	mask->v[offset+2] = (n >> 16);
	mask->v[offset+3] = (n >> 24);
	mask->v[offset+4] = (n >> 32);
	mask->v[offset+5] = (n >> 40);
	mask->v[offset+6] = (n >> 48);
	mask->v[offset+7] = (n >> 56);
}


int koios_tag_fetchname(koios_state *state, koios_tag tag, char **name)
{
	if (!state || !state->bits_per_blocksize || !state->tag_names || !name) {
		return KERR_NULL;
	}
	if (state->tag_names[tag].used) {
		*name = state->tag_names[tag].name;
		return 1;
	}
	return 0;
}


int koios_tag_maskcontains(koios_state *state, koios_mask *mask, koios_tag tag)
{
	size_t i, n;
	uint64_t current;
	if (!state || !state->bits_per_blocksize || !mask || !mask->v) { return KERR_NULL; }
	if (tag > state->bits_per_blocksize) { return KERR_TAGBOUND; }

	i = (tag == 0) ? tag : (tag / 64);
	i *= state->bytes_per_int;
	n = (tag % 64);

	current = load_int(mask, i);
	return !!(current & (1ULL << n));
}


int koios_tag_addtomask(koios_state *state, koios_mask *mask, koios_tag tag)
{
	size_t i, n;
	uint64_t current, new;
	if (!state || !state->bits_per_blocksize || !state->ints_per_blocksize || !mask || !mask->v) {
		return KERR_NULL;
	}
	if (tag > state->bits_per_blocksize) { return KERR_TAGBOUND; }

	i = (tag == 0) ? tag : (tag / 64);
	i *= state->bytes_per_int;
	n = (tag % 64);

	current = load_int(mask, i);
	new = current | (1ULL << n);
	save_int(mask, i, new);

	return 0;
}


int koios_tag_delfrommask(koios_state *state, koios_mask *mask, koios_tag tag)
{
	size_t i, n;
	uint64_t current, new;
	if (!state || !state->bits_per_blocksize || !state->ints_per_blocksize || !mask || !mask->v) {
		return KERR_NULL;
	}
	if (tag > state->bits_per_blocksize) { return KERR_TAGBOUND; }

	i = (tag == 0) ? tag : (tag / 64);
	i *= state->bytes_per_int;
	n = (tag % 64);

	current = load_int(mask, i);
	new = current & ~(1ULL << n);
	save_int(mask, i, new);

	return 0;
}


int koios_mask_new(koios_state *state, koios_mask *mask)
{
	if (!state || !state->bits_per_blocksize || !mask) { return KERR_NULL; }
	if (mask->v) { return KERR_INITVALUE; }

	if (!(mask->v = calloc(state->blocksize, 1))) { return KERR_BADALLOC; }

	return 0;
}


int koios_mask_del(koios_mask *mask)
{
	if (!mask || !mask->v) { return KERR_NULL; }

	free(mask->v);
	mask->v = NULL;

	return 0;
}


int koios_mask_load(koios_state *state, koios_mask *mask, const char *path)
{
	ssize_t size, current_size;
	if (!state || !state->ints_per_blocksize || !state->bytes_per_int || !mask || !mask->v || !path) {
		return KERR_NULL;
	}

	if (((access(path, F_OK)) < 0) || ((access(path, R_OK)) < 0)) { return KERR_NOFIREAD; }

	current_size = state->bytes_per_int*state->ints_per_blocksize;

	if ((size = getxat(path, KOIOS_ATTR_NAME, NULL, 0)) < 0) {
		if (errno == ENOATTR) { return KERR_NOATTR; }
		if (errno == ENOTSUP) { return KERR_NOSUPPORT; }
		return KERR_GETXAT;
	}
	if (size > current_size) { return KERR_BADSIZE; }

	if ((size = getxat(path, KOIOS_ATTR_NAME, mask->v, current_size)) < 0) {
		/* We should not have to check for E(NOATTR/NOTSUP) because we check for it earlier? */
		return KERR_GETXAT;
	}
	if (size < current_size) { return KERR_READSIZE; }

	return 0;
}


int koios_mask_save(koios_state *state, koios_mask *mask, const char *path)
{
	size_t size;
	if (!state || !state->ints_per_blocksize || !state->bytes_per_int || !mask || !mask->v || !path) {
		return KERR_NULL;
	}

	if (((access(path, F_OK)) < 0) || ((access(path, W_OK)) < 0)) { return KERR_NOFIWRITE; }

	size = state->bytes_per_int*state->ints_per_blocksize;

	if ((setxat(path, KOIOS_ATTR_NAME, (char*)mask->v, size, 0)) < 0) { return KERR_SETXAT; }

	return 0;
}


int koios_mask_strip(koios_state *state, const char *path)
{
	if (!state || !path) { return KERR_NULL; }
	if (((access(path, F_OK)) < 0) || ((access(path, W_OK)) < 0)) { return KERR_NOFIWRITE; }

	if ((removexat(path, KOIOS_ATTR_NAME)) < 0) { return KERR_REMXAT; }

	return 0;
}


long long koios_mask_decompose(koios_state *state, koios_mask *mask, koios_tag *tags, long long ntags)
{
	size_t i;
       	long long count, j;
	if (!state || !mask || !mask->v) { return KERR_NULL; }

	for (i = 0, count = 0; i < state->ints_per_blocksize; i++) {
		size_t offset = i*state->bytes_per_int;
		uint64_t value = load_int(mask, offset);
		for (; value; count++) {
			value &= value - 1;
		}
	}

	if (!tags) { return count; }
	if (ntags <= 0 || count > ntags) { return KERR_TAGBUFSIZE; }

	for (i = 0, j = 0; i < state->ints_per_blocksize && j < ntags; i++) {
		int32_t tmp = 0;
		size_t offset = i*state->bytes_per_int;
		uint64_t value = load_int(mask, offset);
		while (value) {
			koios_tag position = 0;
			frexp(value, &tmp); /* Exponent returned is the most-significant bit */
			position = (koios_tag)lrint(tmp); /* TODO: Catch floating point exceptions */
			tags[j++] = (koios_tag)i*64 + (koios_tag)(position-1);
			value ^= (uint64_t)(1ULL << (position-1));
		}
	}

	return j;
}


int koios_mask_compare(koios_state *state, koios_mask *mask, koios_mask *cmp)
{
	size_t i;
	if (!state || !state->ints_per_blocksize || !mask || !mask->v || !cmp || !cmp->v) {
		return KERR_NULL;
	}

	for (i = 0; i < state->ints_per_blocksize; i++) {
		size_t offset = i*state->bytes_per_int;
		uint64_t value0 = load_int(mask, offset);
		uint64_t value1 = load_int(cmp, offset);
		if (value0 != value1) { return 0; }
	}
	return 1;
}


int koios_mask_issubset(koios_state *state, koios_mask *super, koios_mask *sub)
{
	size_t i;
	if (!state || !state->ints_per_blocksize || !super || !sub || !super->v || !sub->v) {
		return KERR_NULL;
	}

	for (i = 0; i < state->ints_per_blocksize; i++) {
		size_t offset = i*state->bytes_per_int;
		uint64_t super_value = load_int(super, offset);
		uint64_t sub_value = load_int(sub, offset);
		if ((super_value & sub_value) != sub_value) {
			return 0;
		}
	}
	return 1;
}


int koios_mask_poptag(koios_state *state, koios_mask *mask, koios_tag *tag)
{
	uint64_t value = 0;
	size_t i, offset = 0;
	for (i = 0; i < state->ints_per_blocksize && !value; i++) {
		offset = i*state->bytes_per_int;
		value = load_int(mask, offset);
	}

	/* There's a subtle off-by-one adjustment we need to make because
	   `i` will increment even if the result in value is positive.
	   Easier and cleaner to document it rather than fix it in the loop */
	i--;

	if (value) {
		int32_t tmp = 0;
		koios_tag position;

		frexp(value, &tmp);
		position = (koios_tag)lrint(tmp);

		*tag = (koios_tag)i*64 + (koios_tag)(position-1);

		value ^= (uint64_t)(1ULL << (position-1));
		save_int(mask, offset, value);

		return 1;
	}
	return 0;
}


int koios_mask_merge(koios_state *state, koios_mask *mask, koios_mask *include)
{
	size_t i;
	if (!state || !state->bytes_per_int || !state->ints_per_blocksize || !mask || !mask->v || !include || !include->v) {
		return KERR_NULL;
	}

	
	for (i = 0; i < state->ints_per_blocksize; i++) {
		size_t offset = i*state->bytes_per_int;
		uint64_t orig = load_int(mask, offset);
		uint64_t inc = load_int(include, offset);
		save_int(mask, offset, (orig | inc));
	}
	return 0;
}


int koios_mask_exclude(koios_state *state, koios_mask *mask, koios_mask *exclude)
{
	size_t i;
	if (!state || !state->bytes_per_int || !state->ints_per_blocksize || !mask || !mask->v || !exclude || !exclude->v) {
		return KERR_NULL;
	}

	for (i = 0; i < state->ints_per_blocksize; i++) {
		size_t offset = i*state->bytes_per_int;
		uint64_t orig = load_int(mask, offset);
		uint64_t exc = load_int(exclude, offset);
		/* orig:   0011
		 * exc:    0101
		 * result: 0010 */
		save_int(mask, offset, (orig & ~exc));
	}
	return 0;
}
