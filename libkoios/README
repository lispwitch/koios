	libkoios
	File tagging system backend
	version 0.0


WHAT IS IT?
	koios is a file tagging system that uses extended attributes,
	supported by many modern file systems, to provide a consistent
	universal tagging system. Because of this reliance, tags are directly
	associated with files and are preserved across copies and moves.

	Unfortunately, because of the nature of varying extended attributes
	support across many different file-systems, it is difficult to ensure
	that copying from one file-system to another will preserve full
	the extended attributes.

	For compactness, tag names are not stored as part of the attributes.
	Instead, a configuration file contains information about the tag names
	and the maxmimum supported attribute size of the filesystem.


INTERFACE
	#include <libkoios.h> to import the interface.

	All functions return zero on success, and a negative number on error,
	except those explicitly marked. Use `koios_errstr` to convert the
	returned error to a human-readable string.


	koios_cfg_*  -- Functions dealing with koios_state

	int koios_cfg_open (koios_state *state, char *path);
		Opens a the given 'state', which is how koios keeps track of
		tag names and the supported size of extended attributes.

	int koios_cfg_close (koios_state *state);
		Frees and closes the 'state'.

	int koios_cfg_store (koios_state *state, char *path);
		Stores the state to disk.

	int koios_cfg_from_native (koios_state *state);
		Creates a new state. Assess the filesystem to determine how
		many tags are supported.

	int koios_cfg_from_given (koios_state *state, size_t blocksize,
	                          size_t version);
		Creates a new state from the given blocksize and koios version.

	int koios_cfg_validate (koios_state *state);
		Checks the given state for discrepancies against the the native
		file system.
		Returns 1 on success, 0 on failure, and negative on error.

	koios_name_*  -- Functions dealing with tag names

	int koios_name_find (koios_state *state, char *name, koios_tag *tag);
		Searches for the given 'name', returns the result in 'tag'.
		Returns 1 on success, 0 if the name does not exist, and
		negative on error.

	int koios_name_set (koios_state *state, char *name, char *newname);
		Searches for 'name', renames it to 'newname'. If 'name' is
		NULL then 'newname' will be added to the list. Returns 1 on
		success, 0 if the name does not exist, and negative on error.

	int koios_name_validate (char *name);
		Validates the given name. Returns a boolean or error.
		Returns 1 on success, 0 if the name does not exist, and
		negative on error.

	int koios_name_search (koios_state *state, koios_tag *tag,
	                       int (*cmp)(char *str, void *data), void *data);
		Iterates through the tag name list, passing each in turn to
		the 'cmp' function, along with a void pointer to 'data'. If
		'cmp' returns true, it will place the named tag in koios_tag,
		and return 1. Otherwise it will return 0, or negative on error.


	const char *koios_errstr (int error);
		Returns a pointer to a human readable error string.
		Returns NULL if the given error does not exist.


	koios_tag_* -- functions dealing primarily with tags

	int koios_tag_fetchname (koios_state *state, koios_tag tag,
	                         char **name);
		Fetch the name associated with the given koios_tag. If the
		given tag exists, place a non-freeable pointer to it in name,
		and return 1; Otherwise return 0, or negative on error.

	int koios_tag_maskcontains (koios_state *state, koios_mask *mask,
	                            koios_tag tag);
		Returns 1 if the given mask contains the tag, 0 if it does not,
		and negative on error.

	int koios_tag_addtomask (koios_state *state, koios_mask *mask,
	                         koios_tag tag);
		Adds the given tag to the mask.

	int koios_tag_delfrommask (koios_state *state, koios_mask *mask,
	                           koios_tag tag);
		Removes the given tag from the mask.


	mask_* -- functions dealing primarily with masks

	int koios_mask_new (koios_state *state, koios_mask *mask);
		Creates a new, pristine mask with no values set. This should be
		run on a mask object before any other mask function are run.

	int koios_mask_del (koios_mask *mask);
		Frees the given mask.

	int koios_mask_load (koios_state *state, koios_mask *mask, char *path);
		Loads the tag mask from a specific file.

	int koios_mask_save (koios_state *state, koios_mask *mask, char *path);
		Saves the tag mask to a specific file.

	int koios_mask_strip (koios_state *state, char *path);
		Strips all tags from the given file.

	int koios_mask_compare (koios_state *state, koios_mask *mask,
	                        koios_mask *cmp);
		Compare two tag masks. Boolean or negative on error.

	int koios_mask_issubset (koios_state *state, koios_mask *super,
	                         koios_mask *sub);
		Check if super contains sub. Boolean or negative on error.

	int koios_mask_poptag (koios_state *state, koios_mask *mask,
	                       koios_tag *tag);
		If a tag is stored in mask, remove it from the mask and place
		it in tag, and return 1. Otherwise, return 0 (or negative on
		error). Please note, unlike almost all of the other operations
		on masks, this is destructive.

	int koios_mask_merge (koios_state *state, koios_mask *mask,
	                      koios_mask *include);
		Ensures all the bits that are set in include are also set in
		mask. Boolean, or negative on error.

	int koios_mask_exclude (koios_state *state, koios_mask *mask,
	                        koios_mask *exclude);
		Ensures all the bits that are set in include are not set in
		mask. Boolean, or negative on error.

	long long koios_mask_decompose (koios_state *state, koios_mask *mask,
	                                koios_tag *tags, long long ntags);
		If tags is NULL, return the number of used tags stored in
		'mask' (i.e. the length of the 'tags' array). Otherwise,
		'decompose' mask into an array of tags located at 'tags'. On
		failure, will return negative for error.


BUILDING
	To build, run `mk`.
	To run with linters, run `mk lint`.


INSTALLATION
	`mk install [DEST=...] [PREFIX=...] [BINDIR=...] [LIBDIR=...] [INCDIR=...]`

	By default:
		DEST=
		PREFIX=/usr/local
		BINDIR=$DEST$PREFIX/bin/
		LIBDIR=$DEST$PREFIX/lib/ or LIBDIR=$DEST$PREFIX/lib64/ (if it exists)
		INCDIR=$DEST$PREFIX/include
		MANDIR=$DEST$PREFIX/man/man3


CONFIG FILE LOCATIONS
	It is recommended to recurse upwards (including the current directory)
	for a file named `.koios.cfg`. Failing that, try the following
	directories in this order:
		$XDG_CONFIG_HOME/.koios.cfg
		$USER/.config/.koios.cfg
		$USER/.koios.cfg
	This is done to allow different mounted file-systems to have different
	sets of tags, and to maintain compatibility with the koios program.


LIBRARIES / DEPENDENCIES
	Building koios requires you to have gcc, mk[0].

	Linting requires scan-build, sparse, and cppcheck to be installed.
	
        [0]: https://9fans.github.io/plan9port/unix/


LICENSE
	BSD 3-Clause Clear License (See LICENSE).


CONTACT / BUGS / FEATURES
	Please please *please* report any bugs to alexandria@inventati.org
	As of yet there are no known bugs.
