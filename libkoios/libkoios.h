#define _XOPEN_SOURCE 700
#define _FORTIFY_SOURCE 2

#include <stdlib.h>
#include <limits.h>
#include <strings.h>
#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <fenv.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifndef ENOATTR
#define ENOATTR ENODATA
#endif

#ifndef RANDOMPATH
#define RANDOMPATH "/dev/urandom"
#endif

#define KOIOS_TAG_NAMSZ 64
#define KOIOS_FORMAT_VERSION 0
#define KOIOS_ATTR_NAME "user.koios.tag"
#define KOIOS_TESTATTR  "user.koios.test"
#define KOIOS_TESTFILE ".koios_test_native"

typedef struct {
	char used;
	char name[KOIOS_TAG_NAMSZ+1];
} koios_name;

typedef struct {
	unsigned char *v;  /* uint64_t v[ints_per_blocksize] */
} koios_mask;

typedef size_t koios_tag;

typedef struct koios_state_s {
	/* provided from the configuration file */
	size_t version;
	size_t blocksize;          /*  in bytes  */

	/* derived */
	size_t bytes_per_int;      /*  sizeof(uint64_t) */
	size_t ints_per_blocksize; /*  blocksize/bytes_per_int (sizeof mask array)  */
	size_t bits_per_blocksize; /*  blocksize*8  (maximum number of tags)  */

	koios_name *tag_names;     /*  tag_names[bits_per_blocksize]  */
} koios_state;

enum koios_err {
	KERR_NULL         = -1,  /* Unexpected NULL */
	KERR_FILEERR      = -2,  /* Error buffering file */
	KERR_BADHEAD      = -3,  /* Bad config header */
	KERR_BADOPTS      = -4,  /* Error while deriving options */
	KERR_BADDIV       = -5,  /* Problem during division */
	KERR_BADALLOC     = -6,  /* Error while requesting more memory */
	KERR_LOADERR      = -7,  /* Error loading config */
	KERR_BADOPEN      = -8,  /* Error while opening file */
	KERR_BADREAD      = -9,  /* Error while reading from file */
	KERR_READSIZE     = -10, /* Not enough data read (from file) */
	KERR_BADFDESC     = -11, /* Invalid file descriptor given */
	KERR_BADBLCKS     = -12, /* Filesystem unable to store attributes */
	KERR_BADNATIV     = -13, /* Error while deriving native options */
	KERR_EMPTYOPTS    = -14, /* options are uninitialized */
	KERR_WRITESIZE    = -15, /* Error while writing to file */
	KERR_NOTFOUND     = -16, /* Unable to find requested tag */
	KERR_BADNAME      = -17, /* Invalid name */
	KERR_MAXTAGS      = -18, /* Maximum number of tags would be exceeded by this operation */
	KERR_NAMESIZE     = -19, /* Size of proposed tag name exceeds builtin name size */
	KERR_TAGBOUND     = -20, /* Tag out of bounds of mask */
	KERR_FAILSTRIP    = -21, /* Failed to strip tags from file */
	KERR_CMPSIZE      = -22, /* Mask sizes are unequal, cannot compare */
	KERR_TAGBUFSIZE   = -23, /* Buffer is too small to hold number of set tags */
	KERR_SETXAT       = -24, /* Setting extended attribute failed */
	KERR_GETXAT       = -25, /* Getting extended attribute failed */
	KERR_REMXAT       = -26, /* Removing extended attribute failed */
	KERR_BADSIZE      = -27, /* Stored tag is larger than default tag value */
	KERR_NOATTR       = -28, /* No tag stored on file */
	KERR_RANDOPEN     = -29, /* Unable to open RANDOMPATH */
	KERR_RANDREAD     = -30, /* Unable to read from RANDOMPATH */
	KERR_NONAME       = -31, /* Name does not exist */
	KERR_NOMKFILE     = -32, /* Unable to create valid KOIOS_TESTFILE */
	KERR_INITVALUE    = -33, /* Tried to initialize an already initialized value */
	KERR_NOSUPPORT    = -34, /* Filesystem does not support extended attributes */
	KERR_NOFIREAD     = -35, /* File does not exist or is not readable */
	KERR_NOFIWRITE    = -36, /* File does not exist or is not writable */
	NUM_KERR          = -37, /*** Maximum number of errors ***/
};


/*  External Interface  */
int     koios_cfg_open         (koios_state *state, const char *path);
int     koios_cfg_close        (koios_state *state);

int     koios_cfg_store        (koios_state *state, const char *path);
int     koios_cfg_from_native  (koios_state *state);
int     koios_cfg_from_given   (koios_state *state, size_t blocksize, size_t version);
int     koios_cfg_validate     (koios_state *state);

int     koios_name_find        (koios_state *state, const char *name, koios_tag *tag);
int     koios_name_set         (koios_state *state, const char *name, const char *newname);
int     koios_name_validate    (const char *name);
int     koios_name_search      (koios_state *state, koios_tag *tag,
                                int (*cmp)(const char *str, void *data), void *data);

const char *koios_errstr       (int error);

int     koios_tag_fetchname    (koios_state *state, koios_tag tag, char **name);
int     koios_tag_maskcontains (koios_state *state, koios_mask *mask, koios_tag tag);
int     koios_tag_addtomask    (koios_state *state, koios_mask *mask, koios_tag tag);
int     koios_tag_delfrommask  (koios_state *state, koios_mask *mask, koios_tag tag);

int     koios_mask_new         (koios_state *state, koios_mask *mask);
int     koios_mask_del         (koios_mask *mask);
int     koios_mask_load        (koios_state *state, koios_mask *mask, const char *path);
int     koios_mask_save        (koios_state *state, koios_mask *mask, const char *path);
int     koios_mask_strip       (koios_state *state, const char *path);
int     koios_mask_compare     (koios_state *state, koios_mask *mask, koios_mask *cmp);
int     koios_mask_issubset    (koios_state *state, koios_mask *super, koios_mask *sub);
int     koios_mask_poptag      (koios_state *state, koios_mask *mask, koios_tag *tag);
int     koios_mask_merge       (koios_state *state, koios_mask *mask, koios_mask *include);
int     koios_mask_exclude     (koios_state *state, koios_mask *mask, koios_mask *exclude);
long long koios_mask_decompose (koios_state *state, koios_mask *mask, koios_tag *tags, long long ntags);
