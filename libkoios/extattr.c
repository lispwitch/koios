#include <sys/types.h>
#include <sys/xattr.h>
#include <stdio.h>
#include <errno.h>
#include "extattr.h"

#ifdef __APPLE__
/* 256KiB */
#define MAXATTRSIZE (262144-1)
#else
#include <linux/limits.h>
#define MAXATTRSIZE XATTR_SIZE_MAX
#endif


int setxat(const char *path, const char *name, const char *value, size_t size, int flags)
{
	#ifdef __APPLE__
	return setxattr(path, name, value, size, (u_int32_t)0, flags);
	#else
	return setxattr(path, name, value, size, flags);
	#endif
}


int lsetxat(const char *path, const char *name, const char *value, size_t size, int flags)
{
	#ifdef __APPLE__
	return setxattr(path, name, value, size, (u_int32_t)0, XATTR_NOFOLLOW|flags);
	#else
	return lsetxattr(path, name, value, size, flags);
	#endif
}


int fsetxat(int fd, const char *name, const char *value, size_t size, int flags)
{
	#ifdef __APPLE__
	return fsetxattr(fd, name, value, size, (u_int32_t)0, flags);
	#else
	return fsetxattr(fd, name, value, size, flags);
	#endif
}


ssize_t getxat(const char *path, const char *name, void *value, size_t size)
{
	#ifdef __APPLE__
	return getxattr(path, name, value, size, (u_int32_t)0, 0);
	#else
	return getxattr(path, name, value, size);
	#endif
}


ssize_t lgetxat(const char *path, const char *name, void *value, size_t size)
{
	#ifdef __APPLE__
	return getxattr(path, name, value, size, (u_int32_t)0, XATTR_NOFOLLOW);
	#else
	return lgetxattr(path, name, value, size);
	#endif
}


ssize_t fgetxat(int fd, const char *name, void *value, size_t size)
{
	#ifdef __APPLE__
	return fgetxattr(fd, name, value, size, (u_int32_t)0, 0);
	#else
	return fgetxattr(fd, name, value, size);
	#endif
}


int removexat(const char *path, const char *name)
{
	#ifdef __APPLE__
	return removexattr(path, name, 0);
	#else
	return removexattr(path, name);
	#endif
}


int lremovexat(const char *path, const char *name)
{
	#ifdef __APPLE__
	return removexattr(path, name, XATTR_NOFOLLOW);
	#else
	return lremovexattr(path, name);
	#endif
}


int fremovexat(int fd, const char *name)
{
	#ifdef __APPLE__
	return fremovexattr(fd, name, 0);
	#else
	return fremovexattr(fd, name);
	#endif
}


int maxxat(void)
{
	return MAXATTRSIZE;
}
