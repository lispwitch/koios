CC             := gcc
CFLAGS         := -static -D_XOPEN_SOURCE=700 -D_FORTIFY_SOURCE=2 -std=c99 -Wall -Wextra -pedantic -O0 -fstack-protector-strong -fno-omit-frame-pointer
CLIBS          :=  -lrt -lm
CPPCHKFLAGS    := --std=posix --std=c99 --platform=unix64 --enable=all --suppress=missingIncludeSystem
SPARSEFLAGS    := -Wsparse-all -gcc-base-dir `gcc --print-file-name=` 
SCANBUILDFLAGS := -enable-checker core -enable-checker security -enable-checker unix
SCANBUILDCMD   := $(shell (command -v scan-build-7) || (command -v scan-build) || echo "" )
SCANBUILD      := $(shell [ -z "$(SCANBUILDCMD)" ] && echo "" || echo "$(SCANBUILDCMD) $(SCANBUILDFLAGS)" )

FILES := extattr.c libkoios.c
OBJS  := $(FILES:.c=.o)
MAN   := libkoios.3
TESTS := test.c

DESTDIR :=
PREFIX  := /usr/local
INCDIR  := $(DESTDIR)$(PREFIX)/include
MANDIR  := $(DESTDIR)$(PREFIX)/man/man3
LIBDIR  := $(shell [ -d $(DESTDIR)$(PREFIX)/lib64/ ] && echo $(DESTDIR)$(PREFIX)/lib64/ || echo $(DESTDIR)$(PREFIX)/lib/ )

.SILENT: build
.PHONY: lint clean

build: libkoios.a
	:

libkoios.a: $(OBJS)
	ar rcs $@ *.o

%.o:
	$(SCANBUILD) $(CC) -c $(CFLAGS) $*.c $(CLIBS)

test: build
	$(SCANBUILD) $(CC) -g $(CFLAGS) $@.c -L. -lkoios $(CLIBS) -o $@
	./$@

lint:
	sparse $(SPARSEFLAGS) $(FILES)
	cppcheck $(CPPCHKFLAGS) --force $(FILES)

clean:
	rm -f *.o *.a tmp_* test

install: libkoios.a libkoios.h
	install -d $(LIBDIR)
	install -m 644 libkoios.a $(LIBDIR)
	install -d $(INCDIR)
	install -m 644 libkoios.h $(INCDIR)
	install -d $(MANDIR)
	install -m 644 $(MAN) $(MANDIR)

uninstall: 
	rm $(LIBDIR)/libkoios.a
	rm $(INCDIR)/libkoios.h
	rm $(MANDIR)/$(MAN)
