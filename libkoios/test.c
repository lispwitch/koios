#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <assert.h>
#include "libkoios.h"

#define passert(a, b, c) \
	passert2(#a " " #b " " #c, a b c)
 
#define passert2(str, cond) { \
	int __result = (cond);  \
	printf("testing %s: (%d)\n", str, __result); \
	assert( __result ); \
}

#define LENGTH(array) sizeof(array)/sizeof(array[0])


int test_koios_mask_file_funs(void)
{
	koios_state K = {0};
	koios_tag T0, T1, T2, T3, T4;
	koios_mask mask = {0};
	koios_mask mask1 = {0};
	koios_mask mask2 = {0};
	static char *testfile = ".TESTFILE";

	passert( koios_cfg_from_given(&K, 2048, KOIOS_FORMAT_VERSION), >=, 0 );
	passert( koios_mask_new(&K, &mask), >=, 0 );
	passert( koios_mask_new(&K, &mask1), >=, 0 );
	passert( koios_mask_new(&K, &mask2), >=, 0 );

	T0 = K.bits_per_blocksize / 3;
	T1 = K.bits_per_blocksize / 2;
	T2 = 2;
	T3 = 8189;
	T4 = K.bits_per_blocksize-1;

	/* Add the tags to the mask */
	passert( koios_tag_addtomask(&K, &mask, T0), >=, 0 );
	passert( koios_tag_addtomask(&K, &mask, T1), >=, 0 );
	passert( koios_tag_addtomask(&K, &mask, T2), >=, 0 );
	passert( koios_tag_addtomask(&K, &mask, T3), >=, 0 );
	passert( koios_tag_addtomask(&K, &mask, T4), >=, 0 );

	/* Double test that we have added them properly yanno */
	passert( koios_tag_maskcontains(&K, &mask, T0), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask, T1), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask, T2), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask, T3), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask, T4), ==, 1 );

	if ((access(testfile, F_OK)) > -1) { unlink(testfile); }

	/* Do some error testing */
	passert( koios_mask_load(&K, &mask, testfile), ==, KERR_NOFIREAD );
	passert( koios_mask_save(&K, &mask, testfile), ==, KERR_NOFIWRITE );
	passert( koios_mask_strip(&K, testfile), ==, KERR_NOFIWRITE );

	/* Make the file */
	passert( open(testfile, O_CREAT|O_RDWR, 0644), >=, 0 );
	
	/* more error testing */
	passert( koios_mask_load(&K, &mask, testfile), ==, KERR_NOATTR );

	/* Test writing */
	passert( koios_mask_save(&K, &mask, testfile), >=, 0 );

	/* Test reading */
	passert( koios_mask_load(&K, &mask1, testfile), >=, 0 );

	/* Test that we have loaded them properly */
	passert( koios_tag_maskcontains(&K, &mask1, T0), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask1, T1), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask1, T2), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask1, T3), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask1, T4), ==, 1 );

	/* Test comparison */
	passert( koios_mask_compare(&K, &mask, &mask1), ==, 1 );

	/* test stripping tags */
	passert( koios_mask_strip(&K, testfile), >=, 0 );
	passert( koios_mask_load(&K, &mask2, testfile), ==, KERR_NOATTR );

	/* Clean up */
	passert( koios_mask_del(&mask), >=, 0 );
	passert( koios_mask_del(&mask1), >=, 0 );
	passert( koios_mask_del(&mask2), >=, 0 );
	passert( unlink(testfile), >=, 0 );
	passert( koios_cfg_close(&K), >=, 0 );

	return 0;
}


int test_koios_mask_funs(void)
{
	koios_state K = {0};
	koios_tag T = 0;
	koios_mask mask = {0};

	passert( koios_cfg_from_given(&K, 2048, KOIOS_FORMAT_VERSION), >=, 0 );
	passert( koios_mask_new(&K, &mask), >=, 0 );

	/* Mask should be empty so this should be false */
	T = 0;
	passert( koios_tag_maskcontains(&K, &mask, T), ==, 0 );

	/* We set the third bit, so the third bit should be set... */
	T = 2;
	passert( koios_tag_addtomask(&K, &mask, T), >=, 0 );
	passert( koios_tag_maskcontains(&K, &mask, T), ==, 1 );

	/* Test the array */
	T = 3; /* Guaranteed to hit another part of the array */
	passert( koios_tag_maskcontains(&K, &mask, T), ==, 0 );
	passert( koios_tag_addtomask   (&K, &mask, T), >=, 0 );
	passert( koios_tag_maskcontains(&K, &mask, T), ==, 1 );
	passert( koios_tag_delfrommask (&K, &mask, T), >=, 0 );
	passert( koios_tag_maskcontains(&K, &mask, T), ==, 0 );

	/* Now let's test bigger than the array! */
	T = K.ints_per_blocksize*2 + 3; /* Guaranteed to hit larger than the array */
	passert( koios_tag_maskcontains(&K, &mask, T), ==, 0 );
	passert( koios_tag_addtomask(&K, &mask, T), >=, 0 );
	passert( koios_tag_maskcontains(&K, &mask, T), ==, 1 );

	/* test bounds errors */
	T = K.bits_per_blocksize;
	passert( koios_tag_maskcontains(&K, &mask, T), ==, 0 ); 
	passert( koios_tag_addtomask(&K, &mask, T), >=, 0 );
	T = K.bits_per_blocksize + 1;
	passert( koios_tag_maskcontains(&K, &mask, T), ==, KERR_TAGBOUND );
	passert( koios_tag_addtomask(&K, &mask, T), ==, KERR_TAGBOUND );

	passert( koios_mask_del(&mask), >=, 0 );
	passert( koios_cfg_close(&K), >=, 0 );

	return 0;
}

int cmp(const char *name, void *data)
{
	const char *target = (const char*)data;
	return (strcmp(name, target)) == 0;
}

int test_koios_name_funs(void)
{
	koios_state K = {0};
	koios_tag T = 0;
	passert( koios_cfg_from_native(&K), >=, 0 );
	
	passert( koios_name_find(&K, NULL, &T), ==, KERR_NULL );
	
	passert( koios_name_set(&K, "name", "new_name"), ==, KERR_NONAME );
	passert( koios_name_set(&K, NULL, "name foo"), ==, KERR_BADNAME );
	
	passert( koios_name_set(&K, NULL, "name"), >=, 0 );
	passert( koios_name_find(&K, "name", &T), >=, 0 );
	passert( T, ==, 0 );
	/* It's the first index that should have been found,
	   there's literally nothing else in the table.
	   Checking this in case we messed up indexes or searching.
	   (Indexes used to be 1-based for a while, so it's a worthy check) */
	
	passert( koios_name_set(&K, NULL, "next_name"), >=, 0 );
	passert( koios_name_find(&K, "next_name", &T), >=, 0 );
	passert( T,  ==, 1 );
	 
	char *s = "unused_string";
	passert( koios_name_search(&K, &T, cmp, (void*)s), ==, 0 );
	char *s1 = "next_name";
	passert( koios_name_search(&K, &T, cmp, (void*)s1), ==, 1 );
	
	passert( koios_cfg_close(&K), >=, 0 );
	
	return 0;
}


int test_koios_decompose(void)
{
	koios_state K = {0};
	koios_tag T[5] = { 0 };
	koios_tag *Tpt = NULL;
	koios_mask mask = {0};
	long long i, ntags;

	passert( koios_cfg_from_given(&K, 2048, KOIOS_FORMAT_VERSION), >=, 0 );
	passert( koios_mask_new(&K, &mask), >=, 0 );

	/* These have to be in order */
	T[0] = 5;
	T[1] = K.bits_per_blocksize / 3;
	T[2] = 8189;
	T[3] = K.bits_per_blocksize / 2;
	T[4] = K.bits_per_blocksize-1;

	/* Add the tags to the mask */
	passert( koios_tag_addtomask(&K, &mask, T[0]), >=, 0 );
	passert( koios_tag_addtomask(&K, &mask, T[1]), >=, 0 );
	passert( koios_tag_addtomask(&K, &mask, T[2]), >=, 0 );
	passert( koios_tag_addtomask(&K, &mask, T[3]), >=, 0 );
	passert( koios_tag_addtomask(&K, &mask, T[4]), >=, 0 );

	/* Double test that we have added them properly yanno */
	passert( koios_tag_maskcontains(&K, &mask, T[0]), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask, T[1]), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask, T[2]), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask, T[3]), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask, T[4]), ==, 1 );

	passert( (ntags = koios_mask_decompose(&K, &mask, NULL, 0)), ==, LENGTH(T) );

	passert( ntags, >, 0 );

	passert( (Tpt = calloc(ntags, sizeof(koios_tag))), !=, NULL );

	passert( koios_mask_decompose(&K, &mask, Tpt, ntags), ==, ntags );

	for (i = 0; i < ntags; i++) {
		fprintf(stderr, "Tpt[%llu]: %lu; T[%llu]: %lu\n", i, Tpt[i], i, T[i]);
		passert( Tpt[i], ==, T[i] );
	}

	free(Tpt);
	passert( koios_mask_del(&mask), >=, 0 );
	passert( koios_cfg_close(&K), >=, 0 );

	return 0;
}


int test_koios_maskcontains(void)
{
	koios_state K = {0};
	koios_tag T[5] = { 0 };
	koios_mask super = {0};
	koios_mask sub   = {0};
	koios_mask nosub = {0};

	passert( koios_cfg_from_given(&K, 2048, KOIOS_FORMAT_VERSION), >=, 0 );
	passert( koios_mask_new(&K, &super), >=, 0 );
	passert( koios_mask_new(&K, &sub), >=, 0 );
	passert( koios_mask_new(&K, &nosub), >=, 0 );

	/* These have to be in order */
	T[0] = 2;
	T[1] = K.bits_per_blocksize / 3;
	T[2] = 4999;
	T[3] = K.bits_per_blocksize / 2;
	T[4] = K.bits_per_blocksize-1;

	/* Add the tags to the superset (only 3 of the 5 tags) */
	passert( koios_tag_addtomask(&K, &super, T[0]), >=, 0 );
	passert( koios_tag_addtomask(&K, &super, T[1]), >=, 0 );
	passert( koios_tag_addtomask(&K, &super, T[2]), >=, 0 );

	/* Add the tags to the subset (only 2 of the 3 superset tags) */
	passert( koios_tag_addtomask(&K, &sub, T[1]), >=, 0 );
	passert( koios_tag_addtomask(&K, &sub, T[2]), >=, 0 );

	/* Add the tags to the non-subset (2 subset tags + 2 non-superset tags) */
	passert( koios_tag_addtomask(&K, &nosub, T[1]), >=, 0 );
	passert( koios_tag_addtomask(&K, &nosub, T[2]), >=, 0 );
	passert( koios_tag_addtomask(&K, &nosub, T[3]), >=, 0 );
	passert( koios_tag_addtomask(&K, &nosub, T[4]), >=, 0 );

	passert( koios_mask_issubset(&K, &super, &sub), ==, 1 );
	passert( koios_mask_issubset(&K, &super, &nosub), ==, 0 );

	passert( koios_mask_del(&super), >=, 0 );
	passert( koios_mask_del(&sub), >=, 0 );
	passert( koios_mask_del(&nosub), >=, 0 );
	passert( koios_cfg_close(&K), >=, 0 );

	return 0;
}


int test_koios_printtags(void)
{
	koios_state K = {0};
	koios_mask mask = {0};
	size_t i = 0;
	size_t n = 5;
	koios_tag T[5] = { 0, 1, 2, 3, 4 };
	char *tagnames[5] = { "foo", "bar", "baz", "qux", "zux" };

	koios_tag t = 0;

	passert( koios_cfg_from_given(&K, 2048, KOIOS_FORMAT_VERSION), >=, 0 );
	passert( koios_mask_new(&K, &mask), >=, 0 );

	for (i = 0; i < n; i++) {
		passert( koios_tag_addtomask(&K, &mask, T[i]), >=, 0 );
		passert( koios_name_set(&K, NULL, tagnames[i]), >=, 0 );
	}

	passert( koios_tag_maskcontains(&K, &mask, T[0]), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask, T[1]), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask, T[2]), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask, T[3]), ==, 1 );
	passert( koios_tag_maskcontains(&K, &mask, T[4]), ==, 1 );

	for (i = 0; i < 5; i++) {
		char *s = NULL;
		passert( koios_mask_poptag(&K, &mask, &t), >, 0 );
		passert( koios_tag_fetchname(&K, t, &s), >=, 0 );
		passert( s, !=, NULL );
		passert( (strcmp(s, tagnames[4-i])), ==, 0 );
	}
	passert( koios_mask_poptag(&K, &mask, &t), ==, 0 );

	passert( koios_mask_del(&mask), >=, 0 );
	passert( koios_cfg_close(&K), >=, 0 );

	return 0;
}

int test_mask_merge_exclude(void)
{
	koios_state K = {0};
	koios_mask mask    = {0};
	koios_mask include = {0};
	koios_mask exclude = {0};
	koios_mask result  = {0};

	passert( koios_cfg_from_given(&K, 2048, KOIOS_FORMAT_VERSION), >=, 0 );
	passert( koios_mask_new(&K, &mask),    >=, 0 );
	passert( koios_mask_new(&K, &include), >=, 0 );
	passert( koios_mask_new(&K, &exclude), >=, 0 );
	passert( koios_mask_new(&K, &result),  >=, 0 );

	koios_tag T[5] = { 2*64+5, 0*64+40, 0*64+3, 2*64+34, 1*64+60 };

	/* mask:    0 1 2 */
	passert( koios_tag_addtomask(&K, &mask, T[0]), >=, 0 );
	passert( koios_tag_addtomask(&K, &mask, T[1]), >=, 0 );
	passert( koios_tag_addtomask(&K, &mask, T[2]), >=, 0 );
	/* include: 2 3 */
	passert( koios_tag_addtomask(&K, &include, T[2]), >=, 0 );
	passert( koios_tag_addtomask(&K, &include, T[3]), >=, 0 );
	/* exclude: 0 4 */
	passert( koios_tag_addtomask(&K, &exclude, T[0]), >=, 0 );
	passert( koios_tag_addtomask(&K, &exclude, T[4]), >=, 0 );
	/* result:  1 2 3 */
	passert( koios_tag_addtomask(&K, &result, T[1]), >=, 0 );
	passert( koios_tag_addtomask(&K, &result, T[2]), >=, 0 );
	passert( koios_tag_addtomask(&K, &result, T[3]), >=, 0 );

	passert( koios_mask_merge(&K, &mask, &include), ==, 0 );
	passert( koios_mask_exclude(&K, &mask, &exclude), ==, 0 );

	passert( koios_mask_compare(&K, &mask, &result), ==, 1 );

	passert( koios_mask_del(&mask), >=, 0 );
	passert( koios_mask_del(&include), >=, 0 );
	passert( koios_mask_del(&exclude), >=, 0 );
	passert( koios_mask_del(&result), >=, 0 );
	passert( koios_cfg_close(&K), >=, 0 );

	return 0;
}


int main(int _, char **__)
{
	(void)__[_-1]; /* shut up, mr compiler */

	test_koios_mask_file_funs();
	test_koios_mask_funs();
	test_koios_name_funs();
	test_koios_decompose();
	test_koios_maskcontains();
	test_koios_printtags();
	test_mask_merge_exclude();
	return 0;
}
