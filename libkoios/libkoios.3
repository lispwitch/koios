.TH libkoios 3 ""
.SH NAME
Koios File Tagging Library (libkoios, \-lkoios)

.SH DESCRIPTION
All functions return zero on success, and a negative number on error,
except those explicitly marked. Use `koios_errstr` to convert the
returned error to a human-readable string.
.P
These functions are segregated into four categories, after the four
central concepts of this library:
.P
A state is a koios_state struct that contains the blocksize 
information and tag name information for the given directory tree.
.P
A name is the stored name of a given tag value.
.P
A tag is a numeric index into a mask.
.P
A mask is fundamentally a block of bits where each bit represents a
tag.

.SH INTERFACE
.B #include <libkoios.h>
.PP
koios_cfg_*  -- Functions dealing with koios_state
.PP
.BI "int koios_cfg_open (koios_state *" state ", char *" path );
.br
	Opens a the given 'state', which is how koios keeps track of
	tag names and the supported size of extended attributes.
.P
.BI "int koios_cfg_close (koios_state *" state );
.br
	Frees and closes the 'state'.
.P
.BI "int koios_cfg_store (koios_state *" state ", char *" path );
.br
	Stores the state to disk.
.P
.BI "int koios_cfg_from_native (koios_state *" state );
.br
	Creates a new state. Assess the filesystem to determine how
	many tags are supported.
.P
.BI "int koios_cfg_from_given (koios_state *" state ", size_t " blocksize ,
.BI "					size_t " version );
.br
	Creates a new state from the given blocksize and koios version.
.P
.BI "int koios_cfg_validate (koios_state *" state );
.br
	Checks the given state for discrepancies against the the native
	file system. Returns 1 on success, 0 on failure, and negative on
	error.
.PP
koios_name_*  -- Functions dealing with tag names
.PP
.BI "int koios_name_find (koios_state * " state ", char * " name ", koios_tag * " tag );
.br
	Searches for the given 'name', returns the result in 'tag'.
	Returns 1 on success, 0 if the name does not exist, and
	negative on error.
.P
.BI "int koios_name_set (koios_state *" state ", char *" name ", char *" newname );
.br
	Searches for 'name', renames it to 'newname'. If 'name' is
	NULL, then 'newname' will be added to the list. Returns 1 on
	success, 0 if the name does not exist, and negative on error.
.P
.BI "int koios_name_validate (char *" name );
.br
	Validates the given name. Returns a boolean or error.
	Returns 1 on success, 0 if the name does not exist, and
	negative on error.
.P
.BI "int koios_name_search (koios_state *" state ", koios_tag *" tag ,
.BI "				int (*" cmp ")(char *" str ", void *" data "), void *" data );
.br
	Iterates through the tag name list, passing each in turn to
	the 'cmp' function, along with a void pointer to 'data'. If
	'cmp' returns true, it will place the named tag in koios_tag,
	and return 1. Otherwise it will return 0, or negative on error.
.P
.P
.BI "const char *koios_errstr (int " error );
.br
	Returns a pointer to a human readable error string. Returns NULL
	if the given error does not exist.
.P
.P
koios_tag_* -- functions dealing primarily with tags
.P
.P
.BI "int koios_tag_fetchname (koios_state * " state ", koios_tag " tag ,
.BI "					char ** " name );
.br
	Fetch the name associated with the given koios_tag. If the given
	tag exists, place a non-freeable pointer to it in name, and
	return 1; Otherwise return 0, or negative on error.
.P
.BI "int koios_tag_maskcontains (koios_state *" state ", koios_mask *" mask ,
.BI "					koios_tag " tag );
.br
	Returns 1 if the given mask contains the tag, 0 if it does not,
	and negative on error.
.P
.BI "int koios_tag_addtomask (koios_state *" state ", koios_mask *" mask ,
.BI "					koios_tag" tag );
.br
	Adds the given tag to the mask.
.P
.BI "int koios_tag_delfrommask (koios_state *state, koios_mask *mask,
.BI "					koios_tag" tag );
.br
	Removes the given tag from the mask.
.PP
mask_* -- functions dealing primarily with masks
.PP
.BI "int koios_mask_new (koios_state *" state ", koios_mask *" mask );
.br
	Creates a new, pristine mask with no values set. This should be
	run on a mask object before any other mask function are run.
.P
.BI "int koios_mask_del (koios_mask *" mask );
.br
	Frees the given mask.
.P
.BI "int koios_mask_load (koios_state *" state ", koios_mask *" mask ", char *" path );
.br
	Loads the tag mask from a specific file.
.P
.BI "int koios_mask_save (koios_state *" state ", koios_mask *" mask ", char *" path );
.br
	Saves the tag mask to a specific file.
.P
.BI "int koios_mask_strip (koios_state *" state ", char *" path );
.br
	Strips all tags from the given file.
.P
.BI "int koios_mask_compare (koios_state *" state ", koios_mask *" mask ,
.BI "					koios_mask *" cmp );
.br
	Compare two tag masks. Boolean or negative on error.
.P
.BI "int koios_mask_issubset (koios_state *" state ", koios_mask *" super ,
.BI "					koios_mask *" sub );
.br
	Check if super contains sub. Boolean or negative on error.
.P
.BI "int koios_mask_poptag (koios_state *" state ", koios_mask *" mask ,
.BI "					koios_tag *" tag );
.br
	If a tag is stored in mask, remove it from the mask and place
	it in tag, and return 1. Otherwise, return 0 (or negative on
	error). Please note, unlike almost all of the other operations
	on masks, this is destructive.
.P
.BI "int koios_mask_merge (koios_state *" state ", koios_mask *" mask ,
.BI "					koios_mask *" include );
.br
	Ensures all the bits that are set in include are also set in
	mask. Boolean, or negative on error.
.P
.BI "int koios_mask_exclude (koios_state *" state ", koios_mask *" mask ,
.BI "					koios_mask *" exclude );
.br
	Ensures all the bits that are set in include are not set in
	mask. Boolean, or negative on error.
.P
.BI "long long koios_mask_decompose (koios_state *" state ", koios_mask *" mask ,
.BI "						koios_tag *" tags ", long long " ntags );
.br
	If tags is NULL, return the number of used tags stored in 'mask'
	(i.e. the length of the 'tags' array). Otherwise, 'decompose'
	mask into an array of tags located at 'tags'. On failure, will
	return negative for error.

.SH LICENSE
BSD 3-Clause Clear License (See \fILICENSE\fR).

.SH AUTHOR / BUGS / FEATURES
.IR libkoios ( 3 )
was written by Alexandria O'Leary
.P
Report any bugs or feature suggestions to
.I alexandria@inventati.org
.P
As of yet there are no known bugs.
.P
Planned features are:
.RS
* Partial config tag includes/links.
