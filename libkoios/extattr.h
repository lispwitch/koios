/*  Linux/OSX portability functions  */
int     setxat     (const char *path, const char *name, const char *value, size_t size, int flags);
int     lsetxat    (const char *path, const char *name, const char *value, size_t size, int flags);
int     fsetxat    (int fd, const char *name, const char *value, size_t size, int flags);

ssize_t getxat     (const char *path, const char *name, void *value, size_t size);
ssize_t lgetxat    (const char *path, const char *name, void *value, size_t size);
ssize_t fgetxat    (int fd, const char *name, void *value, size_t size);

int     removexat  (const char *path, const char *name);
int     lremovexat (const char *path, const char *name);
int     fremovexat (int fd, const char *name);

/* Some extra info */
int     maxxat(void);
