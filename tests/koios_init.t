Change directory to the test dir

  $ cd $TESTDIR/../tmpmnt

Test koios init local directory

  $ ./../koios init .
  $ test -e .koios.cfg
  $ rm .koios.cfg

Test koios init change directory

  $ mkdir foo
  $ ./../koios init -C foo .
  $ test -e foo/.koios.cfg
  $ rm -rf foo

Test koios init respects XDG_CONFIG_HOME

  $ export HOME=. 
  $ export XDG_CONFIG_HOME=$HOME/foo
  $ mkdir foo
  $ ./../koios init
  $ test -e foo/.koios.cfg
  $ rm -rf foo
  $ unset HOME
  $ unset XDG_CONFIG_HOME

Test koios init respects $HOME/.config

  $ export HOME=. 
  $ mkdir .config
  $ ./../koios init
  $ test -e .config/.koios.cfg
  $ rm -rf .config
  $ unset HOME

Test koios init fallback to $HOME

  $ export HOME=. 
  $ ./../koios init
  $ test -e .koios.cfg
  $ rm .koios.cfg
  $ unset HOME

Test koios tagname import

  $ export HOME=.
  $ mkdir foo
  $ ./../koios init .
  $ ./../koios tags +a +b
  $ ./../koios tags
  a
  b
  $ ./../koios init -C foo -i ./../.koios.cfg .
  $ ./../koios tags -C foo
  a
  b
  $ rm -rf foo
  $ rm .koios.cfg
  $ unset HOME

Test koios init import failure

  $ export HOME=.
  $ mkdir foo
  $ ./../koios init -C foo -i ./../koios.cfg .
  Could not include ./../koios.cfg: Error buffering file: No such file or directory
  [1]
  $ rmdir foo
  $ unset HOME

Test koios init change directory failure 

  $ export HOME=.
  $ ./../koios init -C foo -i ./../koios.cfg .
  foo: No such file or directory
  [1]
  $ unset HOME


