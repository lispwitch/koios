
  $ cd $TESTDIR/../tmpmnt

Setup file tree

  $ rm -rf lost+found
  $ mkdir foo
  $ cp ../koios.c foo/testcfile.c
  $ cp ../koios   foo/testprogram
  $ ./../koios init .
  $ ./../koios tags +cfile +binary
  $ ./../koios tags
  cfile
  binary

Ensure files are not tagged then autotag them

  $ ./../koios show .
  $ ./../koios auto text/x-c +cfile .
  $ ./../koios auto application/x-sharedlib +binary .
  $ ./../koios show -t .
  ./foo/testcfile.c: cfile 
  ./foo/testprogram: binary 

Cleanup

  $ rm -rf foo
  $ rm .koios.cfg
