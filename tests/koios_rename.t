Test koios rename

  $ cd $TESTDIR/../tmpmnt

Init koios

  $ ./../koios init .

Inject some tags

  $ ./../koios tags +dummya +dummyb

Verify we injected those tags

  $ ./../koios tags
  dummya
  dummyb

Finally test koios rename

  $ ./../koios rename dummya dummyc

Verify

  $ ./../koios tags
  dummyc
  dummyb

Cleanup

  $ rm .koios.cfg
